/**
 * 
 */
package concurrent;

/**
 * Client.
 * 
 * @author Gabriel Marcano
 * @author Brian
 */
public class Client extends Thread {

	/**
	 * Resource manager (banker) for this client.
	 */
	private final Banker banker;

	/**
	 * Maximum claim of resources.
	 */
	private final int units;

	/**
	 * Number of allocation cycles to go through.
	 */
	private final int requests;

	/**
	 * Minimum sleep time after allocation/release.
	 */
	private final long minSleepMillis;

	/**
	 * Maximum sleep time after allocation/release.
	 */
	private final long maxSleepMillis;

	/**
	 * Initialize the new <code>Client</code> thread object.
	 * 
	 * The name string is passed to the super ({@link Thread}) class's
	 * constructor. The remaining 5 arguments should be saved in instance
	 * variables of the same names for use when the thread is started and
	 * {@link #run()} is called.
	 * 
	 * @param name
	 * @param banker
	 * @param units
	 * @param requests
	 * @param minSleepMillis
	 * @param maxSleepMillis
	 */
	public Client(String name, Banker banker, int units, int requests,
			long minSleepMillis, long maxSleepMillis) {
		super(name);

		this.banker = banker;
		this.units = units;
		this.requests = requests;
		this.minSleepMillis = minSleepMillis;
		this.maxSleepMillis = maxSleepMillis;
	}

	/**
	 * Client action.
	 * 
	 * <ol>
	 * <li>Register a claim for up to <code>units</code> of resource with the
	 * <code>banker</code>.</li>
	 * 
	 * <li>Create a loop that will be executed <code>requests</code> times; each
	 * iteration will either request or release resources by invoking methods in
	 * the <code>banker</code>.</li>
	 * 
	 * <ul>
	 * <li>If <code>banker.remaining == 0</code>, then the client will release
	 * <emph>all</emph> of its units, otherwise the client will request some
	 * units.</li>
	 * 
	 * <li>At the end of each loop, use {@link Thread#sleep(long)} to sleep a
	 * random number of milliseconds from <code>minSleepMillis</code> to
	 * <code>maxSleepMillis</code>. This will introduce another dose of
	 * non-determinism into your program.</li>
	 * </ul>
	 * 
	 * <li>When the loop is done, release any units still allocated and simply
	 * return from {@link #run()} - this will terminate the client thread.</li>
	 * </ol>
	 */
	@Override
	public void run() {
		this.banker.setClaim(this.units);

		for (int i = 0; i < this.requests; i++) {
			int remaining = this.banker.remaining();
			int allocated = this.banker.allocated();

			if (remaining == 0) {
				this.banker.release(allocated);
			} else {
				synchronized (this.banker) {
					this.banker.request(1);
				}
			}

			this.randomSleep();
		}

		int allocated = this.banker.allocated();

		if (allocated > 0)
			this.banker.release(allocated);
	}

	/**
	 * Sleeps a random duration between {@link #maxSleepMillis} and
	 * {@link #minSleepMillis}.
	 */
	private void randomSleep() {
		try {
			long range = this.maxSleepMillis - this.minSleepMillis;
			long duration = (long) (Math.random() * range + this.minSleepMillis);

			Thread.sleep(duration);
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		}
	}
}
