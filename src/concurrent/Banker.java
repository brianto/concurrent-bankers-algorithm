/**
 * 
 */
package concurrent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A banker.
 * 
 * @author Gabriel Marcano
 * @author Brian To
 */
public class Banker {

	/**
	 * Total available units banker has on loan.
	 */
	private final int units;

	/**
	 * Available resources banker has on-hand.
	 */
	private AtomicInteger onHand;

	/**
	 * Mapping of thread to its resource configuration.
	 */
	private ConcurrentHashMap<Thread, ResourceConfiguration> resources;

	/**
	 * Initialize the new Banker to manage <code>units</code> of resource.
	 * 
	 * No threads are registered with the Banker.
	 * 
	 * @param units
	 *            units of resource to allot.
	 */
	public Banker(int units) {
		this.units = units;
		this.onHand = new AtomicInteger(units);

		this.resources = new ConcurrentHashMap<Thread, ResourceConfiguration>();
	}

	/**
	 * Attempt to register a claim for up to <code>units</code> of resource.
	 * 
	 * <ol>
	 * <li>The method calls <code>System.exit(1)</code> if:</li>
	 * 
	 * <ul>
	 * <li>the thread already has a claim registered</li>
	 * <li><code>units</code> is not strictly positive</li>
	 * <li><code>units</code> exceeds the number of resources in the system</li>
	 * </ul>
	 * 
	 * <li>Associate the thread with a current claim equal to <code>units</code>
	 * and a current allocation of 0.</li>
	 * 
	 * <li>Print a message of the form
	 * <code>Thread <emph>name</emph> sets a claim for <emph>units</emph> units.</code>
	 * </li>
	 * </ol>
	 * 
	 * @param units
	 *            Number of units to claim
	 */
	public void setClaim(int units) {
		Thread me = Thread.currentThread();

		if (this.resources.containsKey(me))
			System.exit(1);

		if (units <= 0 || this.units < units)
			System.exit(1);

		this.resources.putIfAbsent(me, new ResourceConfiguration(units));

		System.out.printf("Thread %s sets a claim for %d units.%n",
				me.getName(), units);
	}

	/**
	 * The current thread requests <code>units</code> more resources.
	 * 
	 * <ol>
	 * <li>The method calls <code>System.exit(1)</code> if</li>
	 * 
	 * <ul>
	 * <li>the current thread has no claim registered</li>
	 * <li>units is not strictly positive</li>
	 * <li>units exceeds the invoking thread's remaining claim</li>
	 * </ul>
	 * 
	 * <li>Print the message <code>Thread <emph>name</emph> requests
	 * 		<emph>units</emph> units.</code></li>
	 * 
	 * <li>If allocating the resources results in a safe state,</li>
	 * <ol>
	 * <li>Print a message <code>Thread <emph>name</emph> has
	 * 			<emph>units</emph> units allocated.</code></li>
	 * <li>Update the banker's state and return to the caller</li>
	 * </ol>
	 * 
	 * <li>Otherwise enter a loop that</li>
	 * <ol>
	 * <li>Prints the message <code>Thread <emph>name</emph> waits.</code></li>
	 * <li>Waits for notification of a change.</li>
	 * <li>When reawakened, prints the message
	 * <code>Thread <emph>name</emph> awakened</code>.</li>
	 * <li>If allocating the resources results in a safe state,</li>
	 * <ol>
	 * <li>Print <code>Thread <emph>name</emph> has <emph>units</emph>
	 * 				units allocated.</code></li>
	 * <li>Updates the banker's state and returns.</li>
	 * </ol>
	 * </ol>
	 * 
	 * @param units
	 *            number of units to request
	 * 
	 * @return success status
	 */
	public void request(int units) {
		Thread me = Thread.currentThread();

		if (!this.resources.containsKey(me))
			System.exit(1);

		synchronized (this.resources) {
			int remaining = this.resources.get(me).getRemaining();

			if (units <= 0 || remaining < units)
				System.exit(1);
		}

		System.out
				.printf("Thread %s requests %d units.%n", me.getName(), units);

		boolean success;

		synchronized (this) {
			success = this.tryAllocate(units);
		}

		while (!success) {
			System.out.printf("Thread %s waits.%n", me.getName());

			try {
				synchronized (this) {
					this.wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
				me.interrupt();
			}

			System.out.printf("Thread %s has awakened.%n", me.getName());

			synchronized (this) {
				success = this.tryAllocate(units);
			}
		}
	}

	/**
	 * Checks if proposed state is safe given a thread and requested resources.
	 * 
	 * @param me
	 *            thread requesting the allocation
	 * @param units
	 *            requested allocation amount
	 * @return <code>true</code> if new configuration is safe
	 */
	private boolean willBeSafeWhenAllocating(Thread me, int units) {
		List<ResourceConfiguration> configs = new ArrayList<ResourceConfiguration>(
				this.proposedState(me, units).values());

		Collections.sort(configs);

		int onHand = this.onHand.get();
		for (ResourceConfiguration config : configs) {
			if (config.getRemaining() > onHand)
				return false;

			onHand += config.getAllocated();
		}

		return true;
	}

	/**
	 * Creates a proposed system state with an updated allocation for a thread.
	 * 
	 * This returns a deep copy of the current resources map.
	 * 
	 * @param me
	 *            thread key to update
	 * @param units
	 *            allocation
	 * @return proposed resources state
	 */
	private ConcurrentHashMap<Thread, ResourceConfiguration> proposedState(
			Thread me, int units) {
		ConcurrentHashMap<Thread, ResourceConfiguration> proposed = new ConcurrentHashMap<Thread, ResourceConfiguration>();

		// Copy resource's items to the proposed map
		for (Map.Entry<Thread, ResourceConfiguration> entry : this.resources
				.entrySet()) {
			Thread thread = entry.getKey();
			ResourceConfiguration config = entry.getValue();

			proposed.putIfAbsent(thread, new ResourceConfiguration(config));
		}

		// Update to the proposed state.
		proposed.get(me).allocate(units);

		return proposed;
	}

	/**
	 * Current thread attempts to allocate resources.
	 * 
	 * Helper method.
	 * 
	 * @param units
	 * @return <code>true</code> if successfully allocated
	 */
	private boolean tryAllocate(int units) {
		Thread me = Thread.currentThread();

		if (!this.willBeSafeWhenAllocating(me, units))
			return false;

		this.resources.get(me).allocate(units);
		this.onHand.addAndGet(-units);

		System.out.printf("Thread %s has %d units allocated.%n", me.getName(),
				units);

		return true;
	}

	/**
	 * The current thread releases <code>units</code> resources.
	 * 
	 * <ol>
	 * <li>The method calls <code>System.exit(1)</code> if
	 * <li>
	 * <ul>
	 * <li>the current thread has no claim registered</li>
	 * <li><code>units</code> is not strictly positive</li>
	 * <li><code>units</code> exceeds the number of units allocated to the
	 * current thread</li>
	 * </ul>
	 * 
	 * <li>Print the message <code>Thread <emph>name</emph> releases
	 * 		<emph>units</emph> units</code>.</li>
	 * 
	 * <li>Release <code>units</code> of the units allocated to the current
	 * thread, notify all waiting threads, and return</li>
	 * </ol>
	 * 
	 * @param units
	 *            number of resource units to release.
	 */
	public void release(int units) {
		Thread me = Thread.currentThread();

		if (!this.resources.containsKey(me))
			System.exit(1);

		int allocated = this.resources.get(me).getAllocated();

		if (units <= 0 || allocated < units)
			System.exit(1);

		System.out
				.printf("Thread %s releases %d units.%n", me.getName(), units);

		this.resources.get(me).release(units);
		this.onHand.addAndGet(units);

		synchronized (this) {
			this.notifyAll();
		}
	}

	/**
	 * Returns the number of units allocated to the current thread.
	 * 
	 * @return number of allocated units for this thread.
	 */
	public synchronized int allocated() {
		Thread me = Thread.currentThread();

		return this.resources.get(me).getAllocated();
	}

	/**
	 * Returns the number of units remaining in the current thread's claim.
	 * 
	 * @return number of remaining units available for claim.
	 */
	public int remaining() {
		Thread me = Thread.currentThread();

		return this.resources.get(me).getRemaining();
	}

	/**
	 * Representation of a {@link Client}'s allocated resources.
	 * 
	 * This is essentially a tuple with conveniences for this specific problem.
	 * 
	 * @author Gabriel Marcano
	 * @author Brian
	 */
	private class ResourceConfiguration implements
			Comparable<ResourceConfiguration> {

		/**
		 * Total claim by client.
		 */
		private final int claim;

		/**
		 * Current amount of allocated resources.
		 */
		private AtomicInteger allocated;

		/**
		 * Create a new {@link ResourceConfiguration} with a maximum resource
		 * claim.
		 * 
		 * No resources are allocated on creation.
		 * 
		 * @param claim
		 *            maximum amount of resources {@link Client} may claim
		 */
		private ResourceConfiguration(int claim) {
			this.claim = claim;
			this.allocated = new AtomicInteger(0);
		}

		/**
		 * Creates a new {@link ResourceConfiguration} object from a source.
		 * 
		 * @param source
		 *            {@link ResourceConfiguration} to copy
		 */
		private ResourceConfiguration(ResourceConfiguration source) {
			this.claim = source.claim;
			this.allocated = new AtomicInteger(source.getAllocated());
		}

		/**
		 * Returns the amount of allocated resources.
		 * 
		 * @return amount of allocated resources
		 */
		public int getAllocated() {
			return this.allocated.get();
		}

		public synchronized int getClaim() {
			return this.claim;
		}

		/**
		 * Returns the amount of remaining resources.
		 * 
		 * @return amount of remaining resources
		 */
		public synchronized int getRemaining() {
			return this.claim - this.allocated.get();
		}

		/**
		 * Updates state to reflect an allocation of <code>units</code> units of
		 * resources.
		 * 
		 * @param units
		 *            amount of resources to claim
		 */
		public synchronized void allocate(int units) {
			this.allocated.addAndGet(units);
		}

		/**
		 * Updates state to reflect releasing <code>units</code> units of
		 * resource.
		 * 
		 * @param units
		 *            amount of resources to release
		 */
		public synchronized void release(int units) {
			this.allocated.addAndGet(-units);
		}

		/**
		 * Compare resources by resources remaining.
		 */
		@Override
		public synchronized int compareTo(ResourceConfiguration o) {
			return this.getRemaining() - o.getRemaining();
		}

		/**
		 * Returns a string version of this resource.
		 * 
		 * For testing
		 * 
		 * @return representation of this resource
		 */
		@Override
		public String toString() {
			return String.format("[Resource: %d / %d allocated]",
					this.getAllocated(), this.getClaim());
		}
	}
}
