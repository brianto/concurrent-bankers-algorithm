/**
 * 
 */
package concurrent;

import java.util.HashSet;
import java.util.Set;

/**
 * Entry-point class.
 * 
 * @author Gabriel Marcano
 * @author Brian
 */
public class Driver {

	/**
	 * Number of resources available to banker.
	 */
	public static final int BANKER_UNITS = 3;

	/**
	 * Number of clients.
	 */
	public static final int CLIENT_COUNT = 5;

	/**
	 * Maximum claim for each client.
	 */
	public static final int CLIENT_UNITS = 3;

	/**
	 * Number of times a client requests resources.
	 */
	public static final int CLIENT_REQUESTS = 5;

	/**
	 * Client's minimum sleep time (milliseconds).
	 */
	public static final long MIN_SLEEP = 1000;

	/**
	 * Client's maximum sleep time (milliseconds).
	 */
	public static final long MAX_SLEEP = 5000;

	/**
	 * Program entry point.
	 * 
	 * <ol>
	 * <li>Creates a {@link Banker} object</li>
	 * <li>creates several {@link Client} objects</li>
	 * <li>starts all of the clients</li>
	 * <li>waits for all the clients to complete via the instance method
	 * {@link Thread#join()}</li>
	 * </ol>
	 * 
	 * With proper setting of the configuration parameters you should be able to
	 * produce runs where a {@link Client} is delayed because granting its
	 * request would lead to an unsafe state.
	 * 
	 * The Driver should also contain a set of well-documented final private
	 * static int variables that specify the configuration parameters (number of
	 * resources for the Banker, number of Clients, arguments to the Client
	 * constructors, etc.).
	 * 
	 * @param args
	 *            unused
	 */
	public static void main(String[] args) {
		Banker banker = new Banker(BANKER_UNITS);
		Set<Client> clients = new HashSet<Client>();

		for (int i = 0; i < CLIENT_COUNT; i++)
			clients.add(new Client(String.format("Client %d", i), banker,
					CLIENT_UNITS, CLIENT_REQUESTS, MIN_SLEEP, MAX_SLEEP));

		for (Client c : clients)
			c.start();

		try {
			for (Client c : clients)
				c.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Simulation Completed.");
	}
}
